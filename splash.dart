import 'dart:ffi';
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/loginScreen.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    _navigatetohome;
  }

  _navigatetohome(BuildContext context) async {
    await Future.delayed(Duration(milliseconds: 1500), () {});
    Navigator.pushNamed(context, '/one');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'PHAMBILI',
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
