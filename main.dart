import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/splash.dart';
import 'splash.dart';
import 'loginScreen.dart';
import 'dashboard.dart';
import 'Scholar.dart';
import 'entrepreneur.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool 
    return MaterialApp(
      title: 'PAMBILI',
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromARGB(255, 93, 131, 161),
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: Splash(),
      initialRoute: '/',
      routes: {
        '/': (context) => const Splash(),
        '/one': (context) => loginScreen(),
        '/two': (context) => const dashboard(),
        '/three': (context) => const scholar(),
        '/four': (context) => const entrepreneur(),
      },
    );
  }
}
